"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CartModel = exports.cartSchema = void 0;
const mongoose_1 = require("mongoose");
const ProductModel_1 = require("./ProductModel");
exports.cartSchema = new mongoose_1.Schema({
    customerId: { type: String, required: true },
    products: [ProductModel_1.productSchema],
    summary: { type: Number, required: true },
});
exports.CartModel = (0, mongoose_1.model)("Cart", exports.cartSchema);
//# sourceMappingURL=CartModel.js.map