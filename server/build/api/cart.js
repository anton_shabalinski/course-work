"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cartRouter = void 0;
const express_1 = require("express");
const CartModel_1 = require("../models/CartModel");
const mongoose_1 = require("mongoose");
exports.cartRouter = (0, express_1.Router)();
exports.cartRouter.get("/", (req, res) => {
    (0, mongoose_1.model)("Cart", CartModel_1.cartSchema)
        .findOne({ customerId: req.query.customerId })
        .then((doc) => {
        res.send(doc);
    });
});
exports.cartRouter.post("/", (req, res) => {
    (0, mongoose_1.model)("Cart", CartModel_1.cartSchema)
        .create(Object.assign({}, req.body))
        .then((doc) => {
        res.send(doc);
    });
});
exports.cartRouter.patch("/", (req, res) => {
    (0, mongoose_1.model)("Cart", CartModel_1.cartSchema)
        .findOne({ customerId: req.query.customerId })
        .then((doc) => {
        doc.products.push(...req.body);
        doc.save();
        res.send(doc);
    });
});
//# sourceMappingURL=cart.js.map