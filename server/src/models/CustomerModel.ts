import { Schema, model, Document } from "mongoose";
import { Adress, adressSchema } from "./AdressModel";

export interface Customer extends Document {
  email?: string;
  fullName?: string;
  phone?: string;
  adress?: Adress;
}

export const customerSchema = new Schema<Customer>({
  email: { type: String, required: true },
  fullName: { type: String, required: true },
  phone: { type: String, required: true },
  adress: { type: adressSchema, required: true },
});

export const CustomerModel = model<Customer>("Customer", customerSchema);
