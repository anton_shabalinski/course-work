import { useEffect } from "react";
import { useRouter } from 'next/router'
import { useSelector, useDispatch } from "react-redux";
import { fetchSelectedProduct } from "../../state/slicies/productsSlice";

const Product = () => {
  const router = useRouter();
  const { loading, selectedProduct } = useSelector((state) => state.products);
  const dispatch = useDispatch();

  const productId = router.query.productId

  useEffect(() => {
    dispatch(fetchSelectedProduct(productId));
  }, [productId]);

  if (loading) return <div>Loading...</div>;

  return (
    <div className="w-full h-full flex my-24">
      <div className="">
        <img className="h-96 bg-yellow-300 rounded-xl mr-10" src={`http://localhost:3000/${selectedProduct.imagePath}`} alt={selectedProduct.title} />
      </div>
      <div className="flex-1">
        <p className="uppercase text-gray-400 font-semibold text-md">Артикул: {selectedProduct.article}</p>
        <h1 className="text-6xl text-gray-900 font-bold mb-6">{selectedProduct.title}</h1>
        <p className="text-md text-gray-500">{selectedProduct.description} Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, odit repellendus officia totam autem laborum aliquid eius iste facilis sed quaerat. Maxime tempore, architecto quos eius eos eligendi doloribus optio?
        Illum commodi recusandae quo iure libero inventore. Omnis dolorem ducimus ratione temporibus officiis. Dolorum expedita quis tenetur a. Excepturi est officiis alias ducimus odio dolores aperiam harum sint consectetur nemo?
        Ducimus voluptatem quasi, vel voluptatum, illum unde consequatur accusantium totam earum architecto labore debitis autem? Placeat animi quo maxime, sapiente itaque neque expedita, dicta obcaecati ea, ullam inventore? Porro, error?
        Non quo ea ullam excepturi iusto qui pariatur harum eius laudantium aut dolor eveniet suscipit consequuntur inventore quia, maiores iste dicta voluptatum optio. At a tenetur, iste ea laborum iure!
        Reiciendis, nostrum officia ut vel natus nihil vero eum nam laudantium reprehenderit cumque aliquam, corporis ullam culpa facilis doloribus quia voluptates quisquam repellat accusantium velit architecto. Accusamus sapiente aliquam amet! </p>
        <button onClick={() => {}} className="px-6 py-3 font-medium bg-yellow-300 rounded-xl text-gray-800 mt-9">
          <span>Добавить в корзину {selectedProduct.price} BYN</span>
        </button>
      </div>
    </div>
  )
}

export default Product
