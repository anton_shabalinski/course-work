import { Router, Request, Response } from "express";
import { Customer, customerSchema } from "../models/CustomerModel";
import { model } from "mongoose";

export const customersRouter: Router = Router();

customersRouter.get("/", (req: Request, res: Response) => {
  model("Customer", customerSchema)
    .find()
    .then((doc: Customer[]) => {
      res.send(doc);
    });
});

customersRouter.post("/", (req: Request, res: Response) => {
  model("Customer", customerSchema)
    .create({ ...req.body })
    .then((doc: Customer) => {
      res.send(doc);
    });
});
