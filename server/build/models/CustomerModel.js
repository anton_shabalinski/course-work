"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerModel = exports.customerSchema = void 0;
const mongoose_1 = require("mongoose");
const AdressModel_1 = require("./AdressModel");
exports.customerSchema = new mongoose_1.Schema({
    email: { type: String, required: true },
    fullName: { type: String, required: true },
    phone: { type: String, required: true },
    adress: { type: AdressModel_1.adressSchema, required: true },
});
exports.CustomerModel = (0, mongoose_1.model)("Customer", exports.customerSchema);
//# sourceMappingURL=CustomerModel.js.map