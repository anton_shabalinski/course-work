"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductModel = exports.productSchema = void 0;
const mongoose_1 = require("mongoose");
exports.productSchema = new mongoose_1.Schema({
    article: { type: String, required: true },
    category: { type: String, required: true },
    title: { type: String, required: true },
    description: { type: String, required: true },
    imagePath: String,
    price: { type: Number, required: true },
});
exports.ProductModel = (0, mongoose_1.model)("Product", exports.productSchema);
//# sourceMappingURL=ProductModel.js.map