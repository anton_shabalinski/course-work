"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const compression_1 = __importDefault(require("compression"));
const npmlog_1 = __importDefault(require("npmlog"));
const mongoose_1 = __importDefault(require("mongoose"));
const cors_1 = __importDefault(require("cors"));
const index_1 = __importDefault(require("./config/index"));
const products_1 = require("./api/products");
const customers_1 = require("./api/customers");
const orders_1 = require("./api/orders");
const app = (0, express_1.default)();
app.use((0, compression_1.default)());
app.use((0, cors_1.default)());
app.use("/products", products_1.productsRouter);
app.use("/customers", customers_1.customersRouter);
app.use("/orders", orders_1.ordersRouter);
const initApp = () => {
    try {
        mongoose_1.default
            .connect(index_1.default.dbLink)
            .then(() => {
            app.listen(index_1.default.port, () => {
                npmlog_1.default.info("#Starting >>", `Server started on port ${index_1.default.port}`);
            });
        })
            .catch((err) => {
            npmlog_1.default.error("InitializationError", err);
        });
    }
    catch (error) {
        npmlog_1.default.error("InitializationError", error);
    }
};
initApp();
//# sourceMappingURL=index.js.map