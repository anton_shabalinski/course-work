import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
  loading: false,
  cart: {
    customerId: null,
    products: [],
    summary: 0.00,
  },
};

// export const isNewUser = createAsyncThunk("customers/isNewUser", async () => {
//   if (document.cookie.includes('customerId')) {
//     return false;
//   }
// });

// export const fetchCartData = createAsyncThunk("products/fetchCartData", async (customerId) => {
//   const response = await fetch(`http://localhost:5555/cart/${customerId}`);
//   return response.json();
// });

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  // extraReducers: (builder) => {
  //   builder.addCase(fetchCartData.fulfilled, (state, { payload }) => {
  //     state.cart = payload;
  //     state.loading = false;
  //   });
  //   builder.addCase(fetchCartData.pending, (state) => {
  //     state.loading = true;
  //   });
  // },
});

export default cartSlice;
