import { Router, Request, Response } from "express";
import { Product, productSchema } from "../models/ProductModel";
import { model } from "mongoose";

export const productsRouter: Router = Router();

productsRouter.get("/", (req: Request, res: Response) => {
  model("Product", productSchema)
    .find()
    .then((doc: Product[]) => {
      res.send(doc);
    });
});

productsRouter.get("/:id", (req: Request, res: Response) => {
  model("Product", productSchema)
    .findById(req.params.id)
    .then((doc: Product | any) => {
      res.send(doc);
    });
});

productsRouter.post("/", (req: Request, res: Response) => {
  model("Product", productSchema)
    .create({ ...req.body })
    .then((doc: Product) => {
      res.send(doc);
    });
});

productsRouter.put("/", (req: Request, res: Response) => {
  model("Product", productSchema)
    .findByIdAndUpdate(req.query.editableId, {
      ...req.body,
    })
    .then(() => {
      res.sendStatus(200);
    });
});

productsRouter.delete("/", (req: Request, res: Response) => {
  model("Product", productSchema)
    .findByIdAndDelete(req.params.editableId)
    .then(() => {
      res.sendStatus(200);
    });
});
