import { Schema, model, Document } from "mongoose";
import { Customer, customerSchema } from "./CustomerModel";
import { Product, productSchema } from "./ProductModel";
import { Adress, adressSchema } from "./AdressModel";

export interface Order extends Document {
  customer: Customer;
  products: Product[];
  adress: Adress;
}

export const orderSchema = new Schema<Order>({
  customer: { type: customerSchema, required: true },
  products: { type: [productSchema], required: true },
  adress: { type: adressSchema, required: true },
});

export const OrderModel = model<Order>("Order", orderSchema);
