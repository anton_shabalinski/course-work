"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.customersRouter = void 0;
const express_1 = require("express");
const CustomerModel_1 = require("../models/CustomerModel");
const mongoose_1 = require("mongoose");
exports.customersRouter = (0, express_1.Router)();
exports.customersRouter.get("/", (req, res) => {
    (0, mongoose_1.model)("Customer", CustomerModel_1.customerSchema)
        .find()
        .then((doc) => {
        res.send(doc);
    });
});
exports.customersRouter.post("/", (req, res) => {
    (0, mongoose_1.model)("Customer", CustomerModel_1.customerSchema)
        .create(Object.assign({}, req.body))
        .then((doc) => {
        res.send(doc);
    });
});
//# sourceMappingURL=customers.js.map