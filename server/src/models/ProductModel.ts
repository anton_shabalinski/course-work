import { Schema, model, Document } from "mongoose";

export interface Product extends Document {
  article: string;
  category: string;
  title: string;
  description: string;
  imagePath?: string;
  price: number;
}

export const productSchema = new Schema<Product>({
  article: { type: String, required: true },
  category: { type: String, required: true },
  title: { type: String, required: true },
  description: { type: String, required: true },
  imagePath: String,
  price: { type: Number, required: true },
});

export const ProductModel = model<Product>("Product", productSchema);
