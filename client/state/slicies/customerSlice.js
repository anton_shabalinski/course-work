import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
  customerId: null,
};

export const addCustomer = createAsyncThunk("products/fetchCartData", async (customerId) => {
  const response = await fetch(`http://localhost:5555/customer/${customerId}`);
  return response.json();
});

export const customerSlice = createSlice({
  name: "customer",
  initialState,
  reducers: {
    setCustomer: (state, action) => {
      state.customer = action.payload;
    }
  },
  // extraReducers: (builder) => {
  //   builder.addCase(fetchCartData.fulfilled, (state, { payload }) => {
  //     state.cart = payload;
  //     state.loading = false;
  //   });
  //   builder.addCase(fetchCartData.pending, (state) => {
  //     state.loading = true;
  //   });
  // },
});

export default customerSlice;
