"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.productsRouter = void 0;
const express_1 = require("express");
const ProductModel_1 = require("../models/ProductModel");
const mongoose_1 = require("mongoose");
exports.productsRouter = (0, express_1.Router)();
exports.productsRouter.get("/", (req, res) => {
    (0, mongoose_1.model)("Product", ProductModel_1.productSchema)
        .find()
        .then((doc) => {
        res.send(doc);
    });
});
exports.productsRouter.post("/", (req, res) => {
    (0, mongoose_1.model)("Product", ProductModel_1.productSchema)
        .create(Object.assign({}, req.body))
        .then((doc) => {
        res.send(doc);
    });
});
exports.productsRouter.put("/", (req, res) => {
    (0, mongoose_1.model)("Product", ProductModel_1.productSchema)
        .findByIdAndUpdate(req.query.editableId, Object.assign({}, req.body))
        .then(() => {
        res.sendStatus(200);
    });
});
exports.productsRouter.delete("/", (req, res) => {
    (0, mongoose_1.model)("Product", ProductModel_1.productSchema)
        .findByIdAndDelete(req.params.editableId)
        .then(() => {
        res.sendStatus(200);
    });
});
//# sourceMappingURL=products.js.map