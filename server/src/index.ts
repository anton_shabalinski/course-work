import Express, { Application } from "express";
import Compression from "compression";
import Logger from "npmlog";
import mongoose from "mongoose";
import cors from "cors";
import config from "./config/index";

import { productsRouter } from "./api/products";
import { customersRouter } from "./api/customers";
import { ordersRouter } from "./api/orders";

const app: Application = Express();
app.use(Compression());
app.use(cors());

app.use("/products", productsRouter);
app.use("/customers", customersRouter);
app.use("/orders", ordersRouter);

const initApp = () => {
  try {
    mongoose
      .connect(config.dbLink)
      .then(() => {
        app.listen(config.port, () => {
          Logger.info("#Starting >>", `Server started on port ${config.port}`);
        });
      })
      .catch((err) => {
        Logger.error("InitializationError", err);
      });
  } catch (error: any) {
    Logger.error("InitializationError", error);
  }
};

initApp();
