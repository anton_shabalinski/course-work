"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdressModel = exports.adressSchema = void 0;
const mongoose_1 = require("mongoose");
exports.adressSchema = new mongoose_1.Schema({
    city: { type: String, required: true },
    street: { type: String, required: true },
    house: { type: String, required: true },
    part: String,
    flat: { type: String, required: true },
    entrance: String,
    floor: String,
});
exports.AdressModel = (0, mongoose_1.model)("Adress", exports.adressSchema);
//# sourceMappingURL=AdressModel.js.map