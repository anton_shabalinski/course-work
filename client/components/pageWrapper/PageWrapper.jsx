import React from "react";

const PageWrapper = ({ children }) => {
  return <div className="container px-6 mx-auto">{children}</div>;
};

export default PageWrapper;
