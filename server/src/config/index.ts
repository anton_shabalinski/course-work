const env: string = process.env.NODE_ENV || "development";

export const config: any = {
  development: {
    port: process.env.PORT || 5555,
    dbLink:
      "mongodb+srv://admin:root@toyerby.utlsv.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
  },
  staging: {
    port: process.env.PORT || 5555,
    dbLink:
      "mongodb+srv://admin:root@toyerby.utlsv.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
  },
  production: {
    port: process.env.PORT || 5555,
    dbLink:
      "mongodb+srv://admin:root@toyerby.utlsv.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
  },
};

export default config[env];
