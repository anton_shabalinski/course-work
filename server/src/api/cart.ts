import { Router, Request, Response } from "express";
import { Cart, cartSchema } from "../models/CartModel";
import { model } from "mongoose";

export const cartRouter: Router = Router();

cartRouter.get("/:customerId", (req: Request, res: Response) => {
  model("Cart", cartSchema)
    .findOne({ customerId: req.query.customerId as string })
    .then((doc: Cart | null) => {
      res.send(doc);
    });
});

cartRouter.post("/", (req: Request, res: Response) => {
  model("Cart", cartSchema)
    .create({ ...req.body })
    .then((doc: Cart) => {
      res.send(doc);
    });
});

cartRouter.patch("/", (req: Request, res: Response) => {
  model("Cart", cartSchema)
    .findOne({ customerId: req.query.customerId as string })
    .then((doc: Cart | any) => {
      doc.products.push(...req.body);
      doc.save();
      res.send(doc);
    });
});
