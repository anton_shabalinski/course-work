import React from "react";

import Header from "./Header";
import Footer from "./Footer";
import PageWrapper from "../pageWrapper/PageWrapper";

const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <PageWrapper>{children}</PageWrapper>
      <Footer />
    </>
  );
};

export default Layout;
