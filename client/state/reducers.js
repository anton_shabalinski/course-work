import { combineReducers } from "redux";

import { productsSlice } from "./slicies/productsSlice";
import { customerSlice } from "./slicies/customerSlice";

export const rootReducer = combineReducers({
  [productsSlice.name]: productsSlice.reducer,
  [customerSlice.name]: customerSlice.reducer,
});