"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderModel = exports.orderSchema = void 0;
const mongoose_1 = require("mongoose");
const CustomerModel_1 = require("./CustomerModel");
const ProductModel_1 = require("./ProductModel");
const AdressModel_1 = require("./AdressModel");
exports.orderSchema = new mongoose_1.Schema({
    customer: { type: CustomerModel_1.customerSchema, required: true },
    products: { type: [ProductModel_1.productSchema], required: true },
    adress: { type: AdressModel_1.adressSchema, required: true },
});
exports.OrderModel = (0, mongoose_1.model)("Order", exports.orderSchema);
//# sourceMappingURL=OrderModel.js.map