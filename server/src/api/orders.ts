import { Router, Request, Response } from "express";
import { Order, orderSchema } from "../models/OrderModel";
import { model } from "mongoose";

export const ordersRouter: Router = Router();

ordersRouter.get("/", (req: Request, res: Response) => {
  model("Order", orderSchema)
    .find()
    .then((doc: Order[]) => {
      res.send(doc);
    });
});

ordersRouter.post("/", (req: Request, res: Response) => {
  model("Order", orderSchema)
    .create({ ...req.body })
    .then((doc: Order) => {
      res.send(doc);
    });
});
