"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ordersRouter = void 0;
const express_1 = require("express");
const OrderModel_1 = require("../models/OrderModel");
const mongoose_1 = require("mongoose");
exports.ordersRouter = (0, express_1.Router)();
exports.ordersRouter.get("/", (req, res) => {
    (0, mongoose_1.model)("Order", OrderModel_1.orderSchema)
        .find()
        .then((doc) => {
        res.send(doc);
    });
});
exports.ordersRouter.post("/", (req, res) => {
    (0, mongoose_1.model)("Order", OrderModel_1.orderSchema)
        .create(Object.assign({}, req.body))
        .then((doc) => {
        res.send(doc);
    });
});
//# sourceMappingURL=orders.js.map