import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
  loading: false,
  selectedProduct: {},
  products: [],
};

export const fetchProducts = createAsyncThunk("products/fetchProducts", async () => {
  const response = await fetch("http://localhost:5555/products");
  return response.json();
});

export const fetchSelectedProduct = createAsyncThunk("products/fetchSelectedProduct", async (id) => {
  const response = await fetch(`http://localhost:5555/products/${id}`);
  return response.json();
});

export const productsSlice = createSlice({
  name: "products",
  initialState,
  extraReducers: (builder) => {
    builder.addCase(fetchProducts.fulfilled, (state, { payload }) => {
      state.products = payload;
      state.loading = false;
    });
    builder.addCase(fetchProducts.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchSelectedProduct.fulfilled, (state, { payload }) => {
      state.selectedProduct = payload;
      state.loading = false;
    });
    builder.addCase(fetchSelectedProduct.pending, (state) => {
      state.loading = true;
    });
  },
});

export default productsSlice;
