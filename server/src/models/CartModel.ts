import { Schema, model, Document } from "mongoose";
import { Product, productSchema } from "./ProductModel";

export interface Cart extends Document {
  customerId: string;
  products: Product[];
  summary: number;
}

export const cartSchema = new Schema<Cart>({
  customerId: { type: String, required: true },
  products: [productSchema],
  summary: { type: Number, required: true },
});

export const CartModel = model<Cart>("Cart", cartSchema);
