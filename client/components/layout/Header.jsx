/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from "react";
import Image from "next/image";
import Link from "next/link";
import { Popover, Transition } from "@headlessui/react";
import { ShoppingBagIcon } from "@heroicons/react/outline";
import PageWrapper from "../pageWrapper/PageWrapper";

import toyerLogo from "../../public/logo.svg";

const Header = () => {
  return (
    <PageWrapper>
      <div className="flex w-full justify-between items-center py-6">
        <Link href="/">
          <Image className="h-8 w-auto sm:h-10" src={toyerLogo} alt="" />
        </Link>

        <Link
          href="/catalog"
          className="text-base font-base text-gray-500 hover:text-gray-900"
        >
          Каталог
        </Link>
        <Link
          href="/delivery"
          className="text-base font-base text-gray-500 hover:text-gray-900"
        >
          Оплата и доставка
        </Link>
        <Link
          href="/about"
          className="text-base font-base text-gray-500 hover:text-gray-900"
        >
          О нас
        </Link>
        <Link
          href="/contacts"
          className="text-base font-base text-gray-500 hover:text-gray-900"
        >
          Контакты
        </Link>
        <Link
          href="/cart"
          className="text-base font-base text-gray-500 hover:text-gray-900"
        >
          <div className="flex items-center justify-center flex-col">
            <ShoppingBagIcon className="h-8 w-8 pb-1" />
            <span className="text-xs">{`${24.75} BYN`}</span>
          </div>
        </Link>
      </div>
    </PageWrapper>
  );
};

export default Header;
