import { Schema, model, Document } from "mongoose";

export interface Adress extends Document {
  city: string;
  street: string;
  house: string;
  part?: string;
  flat: string;
  entrance?: string;
  floor?: string;
}

export const adressSchema = new Schema<Adress>({
  city: { type: String, required: true },
  street: { type: String, required: true },
  house: { type: String, required: true },
  part: String,
  flat: { type: String, required: true },
  entrance: String,
  floor: String,
});

export const AdressModel = model<Adress>("Adress", adressSchema);
